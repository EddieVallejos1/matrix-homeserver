# Campgrounds Synapse

This is a fork of the [Matrix Synapse](https://github.com/matrix-org/synapse) repo.
This will be used by the Campgrounds homeserver as the base image.

## Pulling Changes

The `upstream` remote is the original Synapse repo. Always pull updates from the
latest release branch:
```
git fetch --all --tags --prune
```

Then merge to `origin` on `base` branch:
```
git checkout base
git merge tags/v1.21.1
```

## Build and Deploy

Build then upload the image to the container registry:
```
docker build -t asia.gcr.io/campgrounds-dev-7e6fe/campgrounds-synapse -f docker/Dockerfile .
docker push asia.gcr.io/campgrounds-dev-7e6fe/campgrounds-synapse
```
