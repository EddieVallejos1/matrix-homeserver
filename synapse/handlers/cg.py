# -*- coding: utf-8 -*-
# Copyright 2014-2016 OpenMarket Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import logging
import random

from synapse.api.errors import (
    AuthError,
    Codes,
    HttpResponseException,
    RequestSendFailed,
    StoreError,
    SynapseError,
)
from synapse.metrics.background_process_metrics import run_as_background_process
from synapse.types import UserID, create_requester, get_domain_from_id

from ._base import BaseHandler

logger = logging.getLogger(__name__)


class CgHandler(BaseHandler):
  def __init__(self, hs):
    super().__init__(hs)

  async def cg_get_roles(self, room_id):
    try:
      roles = await self.store.cg_get_roles(room_id)
    except StoreError as e:
      if e.code == 404:
        raise SynapseError(404, "Room was not found", Codes.NOT_FOUND)
      elif e.code == 400:
        raise SynapseError(400, "Invalid params", Codes.INVALID_PARAM)

    return roles

  async def cg_join_group_by_link(self, link, user_id):
    try:
      row = await self.store.cg_get_group_id_by_link(link)

      if await self.store.cg_get_group_user(row["group_id"], user_id):
        raise SynapseError(403, "User is already a member of the group " + row["group_id"], "ALREADY_A_MEMBER")

      await self.store.cg_add_user_to_group(row["group_id"], user_id)
      await self.store.cg_add_user_to_local_group_membership(row["group_id"], user_id)

      group = await self.store.get_group(row["group_id"])

      return group
      
    except StoreError as e:
      if e.code == 404:
        raise SynapseError(404, "Link is either expired or invalid.", Codes.NOT_FOUND)
      elif e.code == 400:
        raise SynapseError(400, "Invalid params", Codes.INVALID_PARAM)

    return row["group_id"]

  async def cg_get_invite_link(self, group_id):
    try:
      invite_link = await self.store.cg_get_invite_link(group_id)
    except StoreError as e:
      if e.code == 404:
        raise SynapseError(404, "Group was not found", Codes.NOT_FOUND)
      elif e.code == 400:
        raise SynapseError(400, "Invalid params", Codes.INVALID_PARAM)

    return invite_link

  async def cg_create_role(self, room_id, name, power_level):
    try:
      await self.store.cg_create_role(room_id, name, power_level)
    except StoreError as e:
      if e.code == 404:
        raise SynapseError(404, "Room was not found", Codes.NOT_FOUND)
      elif e.code == 400:
        raise SynapseError(400, "Invalid params", Codes.INVALID_PARAM)

  async def cg_update_is_admin(self, group_id, user_id, is_admin, requester_user_id):
    try:
      await self.store.cg_update_is_admin(group_id, user_id, is_admin)
    except StoreError as e:
      if e.code == 404:
        raise SynapseError(404, "Room was not found", Codes.NOT_FOUND)
      elif e.code == 400:
        raise SynapseError(400, "Invalid params", Codes.INVALID_PARAM)
  
  async def cg_get_contacts(self, search, user_id):
    try:
      contacts = await self.store.cg_get_contacts(search, user_id)

      user_profiles = []

      for entry in contacts:
        user_id = entry["user_id"]
        try:
          profile = await self.store.cg_get_profile(user_id)
          user_profiles.append({
            "displayname": profile["displayname"],
            "avatar_url": profile["avatar_url"],
            "group_id": entry["group_id"],
            "user_id": user_id
          })
        except Exception as e:
          logger.warning("Error getting profile for %s: %s", user_id, e)


      return {"chunk": user_profiles, "total_user_count_estimate": len(user_profiles)}
    except StoreError as e:
      if e.code == 404:
        raise SynapseError(404, "User was not found", Codes.NOT_FOUND)
      elif e.code == 400:
        raise SynapseError(400, "Invalid params", Codes.INVALID_PARAM)

