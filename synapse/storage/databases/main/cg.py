# -*- coding: utf-8 -*-
# Copyright 2014-2016 OpenMarket Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from typing import Any, Dict, List

from synapse.api.errors import StoreError
from synapse.storage._base import SQLBaseStore
from synapse.storage.databases.main.roommember import ProfileInfo
from synapse.types import UserID

class CgStore(SQLBaseStore):
  async def cg_get_roles(self, room_id: str) -> List[Dict[str, Any]]:
    return await self.db_pool.simple_select_list(
      table="cg_roles",
      keyvalues={"room_id": room_id},
      retcols=["name", "power_level"],
    )

  async def cg_get_invite_link(self, group_id: str):
    return await self.db_pool.simple_select_one(
      table="invite_links",
      keyvalues={"group_id": group_id},
      retcols=["link", "group_id", "expiry"]
    )

  async def cg_get_group_id_by_link(self, link: str):
    return await self.db_pool.simple_select_one(
      table="invite_links",
      keyvalues={"link": link},
      retcols=["group_id"]
    )

  async def cg_get_group_user(self, group_id: str, user_id: str):
    return await self.db_pool.simple_select_one(
      table="group_users",
      keyvalues={"group_id": group_id, "user_id": user_id},
      retcols=["group_id", "user_id"],
      allow_none=True
    )

  async def cg_add_user_to_group(self, group_id: str, user_id: str) -> None:
    await self.db_pool.simple_insert(
      table="group_users",
      values={
        "group_id": group_id,
        "user_id": user_id,
        "is_admin": False,
        "is_public": False
      },
      desc="add_user_to_group",
    )

  async def cg_add_user_to_local_group_membership(self, group_id: str, user_id: str) -> None:
    await self.db_pool.simple_insert(
      table="local_group_membership",
      values={
        "group_id": group_id,
        "user_id": user_id,
        "is_admin": False,
        "membership": "join",
        "is_publicised": False,
        "content": "{}"
      },
      desc="add_user_to_local_group_membership",
    )

  async def cg_create_role(self, room_id: str, name: str, power_level: int) -> None:
    await self.db_pool.simple_insert(
      table="cg_roles",
      values={"room_id": room_id, "name": name, "power_level": power_level},
      desc="create_role",
    )

  async def cg_update_is_admin(self, group_id: str, user_id: str, is_admin: bool) -> None:
    await self.db_pool.simple_update(
      table="group_users",
      keyvalues={"group_id": group_id, "user_id": user_id},
      updatevalues={"is_admin": is_admin},
      desc="update_group_is_admin",
    )

  async def cg_get_profile(self, user_id: str):
    return await self.db_pool.simple_select_one(
      table="profiles",
      keyvalues={"user_id": UserID.from_string(user_id).localpart},
      retcols=["displayname", "avatar_url"],
      desc="cg_get_profile",
    )
  
  async def get_group(self, group_id: str):
      return await self.db_pool.simple_select_one(
          table="groups",
          keyvalues={"group_id": group_id},
          retcols=(
            "name",
            "short_description",
            "long_description",
            "avatar_url",
            "is_public",
            "join_policy",
            "group_id"
          ),
          allow_none=True,
          desc="get_group",
      )

  async def cg_get_contacts(self, search: str, requester_user_id: str):
    return await self.db_pool.simple_search_with_filter(
      table= "group_users",
      searchterm= search,
      searchcol= "user_id",
      retcols= ["group_id", "user_id"],
      subquerycol= "group_id",
      subfiltercol= "user_id",
      subfilterval= requester_user_id
    )