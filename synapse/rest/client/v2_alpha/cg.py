# -*- coding: utf-8 -*-
# Copyright 2017 Vector Creations Ltd
# Copyright 2018 New Vector Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import logging

from synapse.api.errors import Codes, SynapseError
from synapse.http.servlet import RestServlet, parse_json_object_from_request, parse_string
from ._base import client_patterns
from synapse.linkpreview import link_preview
import datetime
import json

logger = logging.getLogger(__name__)

class CgRolesRestServlet(RestServlet):
  PATTERNS = client_patterns(
    "/cg/rooms/(?P<room_id>[^/]*)/roles$", releases=(), unstable=True
  )

  def __init__(self, hs):
    super().__init__()
    self.hs = hs
    self.cg_handler = hs.get_cg_handler()

  async def on_GET(self, request, room_id):
    roles = await self.cg_handler.cg_get_roles(room_id)

    ret = {"roles": roles}
    return 200, ret

  async def on_POST(self, request, room_id):
    content = parse_json_object_from_request(request)

    try:
      name = content["name"]
      power_level = content["power_level"]
    except KeyError:
      raise SynapseError(
        400, "Missing key 'name', 'power_level'", errcode=Codes.MISSING_PARAM
      )

    if power_level < 1 or power_level > 100:
      raise SynapseError(
        400, "Power level values should be: [1, 100]"
      )

    await self.cg_handler.cg_create_role(room_id, name, power_level)

    return 200, {}

class CgGroupsRestServlet(RestServlet):
  PATTERNS = client_patterns(
    "/cg/groups/(?P<group_id>[^/]*)/members/(?P<user_id>[^/]*)", releases=(), unstable=True
  )

  def __init__(self, hs):
    super().__init__()
    self.auth = hs.get_auth()
    self.cg_handler = hs.get_cg_handler()

  async def on_GET(self, request, group_id, user_id):
    requester = await self.auth.get_user_by_req(request)
    requester_user_id = requester.user.to_string()

    content = parse_json_object_from_request(request)

    try:
      is_admin = content["is_admin"]
    except KeyError:
      raise SynapseError(
        400, "Missing key 'is_admin'", errcode=Codes.MISSING_PARAM
      )

    await self.cg_handler.cg_update_is_admin(group_id, user_id, is_admin, requester_user_id)

    return 200, {}

class CgLinkPreviewRestServlet(RestServlet):
  PATTERNS = client_patterns(
    "/cg/link-preview$", releases=(), unstable=True
  )

  def __init__(self, hs):
    super().__init__()
    self.auth = hs.get_auth()
    self.cg_handler = hs.get_cg_handler()

  async def on_POST(self, request):
    requester = await self.auth.get_user_by_req(request)
    requester_user_id = requester.user.to_string()

    content = parse_json_object_from_request(request)

    try:
      url = content["url"]
    except KeyError:
      raise SynapseError(
        400, "Missing key 'url'", errcode=Codes.MISSING_PARAM
      )

    preview = link_preview(url)

    return 200, {
      "title": preview.title,
      "description": preview.description,
      "image": preview.image,
      "force_title": preview.force_title,
      "absolute_image": preview.absolute_image,
      "site_name": preview.site_name
    }

class CgCampInviteLinksRestServlet(RestServlet):
  PATTERNS = client_patterns(
    "/cg/groups/(?P<group_id>[^/]*)/invite-link$", releases=(), unstable=True
  )

  def __init__(self, hs):
    super().__init__()
    self.auth = hs.get_auth()
    self.cg_handler = hs.get_cg_handler()

  async def on_GET(self, request, group_id):
    invite_link = await self.cg_handler.cg_get_invite_link(group_id)
    
    return 200, {
      "link": invite_link["link"],
      "group_id": invite_link["group_id"],
      "expiry": invite_link["expiry"].timestamp()
    }


class CgVerifyInviteLinkRestServlet(RestServlet):
  PATTERNS = client_patterns(
    "/cg/groups/invite-by-link$", releases=(), unstable=True
  )

  def __init__(self, hs):
    super().__init__()
    self.hs = hs
    self.auth = hs.get_auth()
    self.cg_handler = hs.get_cg_handler()

  async def on_POST(self, request):
    content = parse_json_object_from_request(request)

    try:
      link = content["link"]
    except KeyError:
      raise SynapseError(
        400, "Missing key 'link'", errcode=Codes.MISSING_PARAM
      )

    requester = await self.auth.get_user_by_req(request)
    requester_user_id = requester.user.to_string()

    group = await self.cg_handler.cg_join_group_by_link(link, requester_user_id)

    return 200, group

class CgContactsRestServlet(RestServlet):
  PATTERNS = client_patterns(
    "/cg/contacts$", releases=(), unstable=True
  )

  def __init__(self, hs):
    super().__init__()
    self.auth = hs.get_auth()
    self.cg_handler = hs.get_cg_handler()

  async def on_GET(self, request):
    requester = await self.auth.get_user_by_req(request)
    requester_user_id = requester.user.to_string()
    search = parse_string(request, "search")
    contacts = await self.cg_handler.cg_get_contacts(search, requester_user_id)
    
    return 200, contacts

def register_servlets(hs, http_server):
  CgRolesRestServlet(hs).register(http_server)
  CgGroupsRestServlet(hs).register(http_server)
  CgLinkPreviewRestServlet(hs).register(http_server)
  CgCampInviteLinksRestServlet(hs).register(http_server)
  CgVerifyInviteLinkRestServlet(hs).register(http_server)
  CgContactsRestServlet(hs).register(http_server)
